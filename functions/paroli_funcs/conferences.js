const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.database();
const env = functions.config();
const { v4: uuidv4 } = require('uuid');
const moment = require("moment");

async function AddCallJob(ConferenceId, Task, ParticipantNumber = null, Uuid = null, ConfMemberId = null, IsModerator = null, IsManualCall = null)
{
  let job = {
    'ConferenceId': ConferenceId,
    'Status': 'ready',
    'Task': Task,
    'IsManualCall': IsManualCall
  }

  if (ParticipantNumber !== null) job.Number = ParticipantNumber;
  if (Uuid !== null) job.Uuid = Uuid;
  if (ConfMemberId !== null) job.ConfMemberId = ConfMemberId;
  if (IsModerator !== null) job.IsModerator = IsModerator;
  if (IsManualCall !== null) job.IsManualCall = IsManualCall;

  return await db.ref('Calljobs/New').push(job);
}

const requestCallParticipant = async (conferenceId, phoneNumber, manualCall) =>
{

  if (!conferenceId || !phoneNumber)
  {
    throw new Error("Requires conference ID and new participant's phone number.")
  }

  let foundConf = await findConf(conferenceId);

  if (!foundConf)
  {
    throw new Error("Conference not found with ID " + conferenceId);
  }

  let thisConf = foundConf.res.val();

  // check to see if the participant is already in the call
  if (thisConf.Participants && thisConf.Participants[phoneNumber] &&
    thisConf.Participants[phoneNumber].SessionData && thisConf.Participants[phoneNumber].SessionData.InCall)
  {
    return true;
  }

  // Unban the user, if they are banned
  if (thisConf.Participants[phoneNumber].Banned)
  {
    await db.ref(foundConf.path).child(`/Participants/${phoneNumber}/Banned`).set(null);
  }

  await AddCallJob(conferenceId, 'call', phoneNumber, null, null, thisConf.Participants[phoneNumber].Moderator, manualCall);

  return true;
}

async function getEntryPIN()
{
  let pinsInUse = [];

  let scheduled = await db.ref("Conferences/Upcoming").once("value");
  let live = await db.ref("Conferences/Live").once("value");

  if (scheduled.exists())
  {
    for (let conf in scheduled.val())
    {
      pinsInUse.push(conf.PIN)
    }
  }

  if (live.exists())
  {
    for (let conf in live.val())
    {
      pinsInUse.push(conf.PIN)
    }
  }

  function createPin()
  {
    return Math.random().toString().substr(2, 4)
  }

  let newPin = createPin()

  while (pinsInUse.includes(newPin))
  {
    newPin = createPin();
  }

  return newPin;
}


async function findConf(confId)
{
  var thisConf = await db.ref(`Conferences/Live/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Live/${confId}`, 'res': thisConf };

  thisConf = await db.ref(`Conferences/Upcoming/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Upcoming/${confId}`, 'res': thisConf };

  thisConf = await db.ref(`Conferences/Complete/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Complete/${confId}`, 'res': thisConf };

  return null;
}

// Get upcoming conferences to avoid clashes
// Done through Functions rather than binding as user accounts can't access each other's scheduled conferences
exports.getUpcomingConferences = functions.https.onCall(async (data, context) => 
{
  if (!context.auth || !context.auth.uid)
  {
    throw new Error("Not logged in.")
  }

  let upcoming = await db.ref('Conferences/Upcoming').once('value');
  let live = await db.ref('Conferences/Live').once('value');

  upcoming = upcoming.exists() ? upcoming.val() : {};
  live = live.exists() ? live.val() : {};

  let toRet = {...upcoming, ...live};

  for (const id in toRet) {
    toRet[id] = {
      'ScheduledStart' : toRet[id].ScheduledStart,
      'ScheduledEnd' : toRet[id].ScheduledEnd,
      'DropCallJoinNumber' : toRet[id].DropCallJoinNumber,
      'MaxCapacity' : toRet[id].MaxCapacity,
      'LengthMins' : toRet[id].LengthMins,
    }
  }

  return toRet;
});

// check for conferences that should be marked as complete or live
// runs on the hour and at half past, every hour (calls work in 30 min slots)
// end live conferences before starting new ones to ensure we have enough capacity
exports.checkForBeginAndEnd = functions.pubsub.schedule('0,15,30,45 * * * *').onRun(async (context) =>
{
  console.log('Checking for conferences which should be ended...');
  const liveConfs = await db.ref(`Conferences/Live`).once("value");

  let liveConfsPromises = [];
  let upcomingConfsPromises = [];
  let now = moment();

  if (liveConfs.exists())
  {
    let liveVals = liveConfs.val();
    for (let confId in liveVals)
    {
      let thisConf = liveVals[confId];
      if (!thisConf.ScheduledEnd || moment(thisConf.ScheduledEnd) < now)
      {
        console.log('Ending conference', thisConf.Name)
        liveConfsPromises.push(AddCallJob(confId, 'endConf'));
      }
    }
  }

  if (liveConfsPromises.length === 0)
  {
    console.log('No conferences to end')
  }
  await Promise.allSettled(liveConfsPromises);

  console.log('Checking for any upcoming conferences which should start...');
  const upcomingConfs = await db.ref(`Conferences/Upcoming`).once("value");

  if (upcomingConfs.exists())
  {
    let upcomingVals = upcomingConfs.val();
    for (let confId in upcomingVals)
    {
      let thisConf = upcomingVals[confId];
      if (moment(thisConf.ScheduledStart) <= now)
      {
        console.log('Starting conference', thisConf.Name)
        upcomingConfsPromises.push(startGivenConference(confId, false));
      }
    }
  }

  if (upcomingConfsPromises.length === 0)
  {
    console.log('No conferences to start')
  }
  await Promise.allSettled(upcomingConfsPromises);

  return null;
});

/**
 * Schedule a conference
 *
 * Client must pass:
 * Name - name of the conference
 * ScheduledStart - the conference's scheduled start time (this is optional)
 * Privacy - the privacy settings for this conference
 * Participants - the participants for the scheduled meeting
 */
exports.scheduleConference = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    let scheduledStart = data.ScheduledStart || Date.now();

    let newConference = {
      "Name": data.Name,
      "DropCallJoinNumber": data.DropCallJoinNumber ?? env.deployment.number,
      "PIN": await getEntryPIN(),
      "Created": Date.now(),
      "ScheduledStart": scheduledStart,
      "ScheduledEnd": moment(scheduledStart).add(data.LengthMins, 'minutes').valueOf(),
      "Owner": context.auth.uid,
      "Privacy": data.Privacy,
      "Participants": data.Participants,
      "ShareLinkPwd": uuidv4(),
      "LengthMins": data.LengthMins,
      "MaxCapacity": data.MaxCapacity
    }

    if (data.Agenda) newConference.Agenda = data.Agenda;

    const newRef = await db.ref("Conferences/Upcoming").push(newConference);
    return newRef.key;
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError("internal", err);
  }
});

/**
 * Update an upcoming conference. You can update the ScheduledStart, Name and Privacy
 * of the conference
 *
 * Client must pass:
 * ConferenceId - the id of the upcoming conference to update
 *
 * Client must pass zero or more of:
 * Name - name of the conference
 * ScheduledStart - the conference's scheduled start time (this is optional)
 * Privacy - the privacy settings for this conference
 */
exports.updateConference = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid) throw new Error("Not logged in.");

    const confRef = db.ref("Conferences/Upcoming").child(data.ConferenceId);

    const currentConferenceSnapshot = await confRef.once("value");

    if (!currentConferenceSnapshot.exists())
      throw new Error("This Conference doesn't exist");

    const currentConference = currentConferenceSnapshot.val();

    let newStart = data.ScheduledStart ?? currentConference.ScheduledStart;
    let newLength = data.LengthMins ?? currentConference.LengthMins

    return await confRef.update({
      Name: data.Name ?? currentConference.Name,
      Privacy: data.Privacy ?? currentConference.Privacy,
      ScheduledStart: newStart,
      LengthMins: newLength,
      ScheduledEnd: moment(newStart).add(newLength, 'minutes').valueOf(),
      MaxCapacity: data.MaxCapacity ?? currentConference.MaxCapacity,
      DropCallJoinNumber: data.DropCallJoinNumber ?? currentConference.DropCallJoinNumber
    });
  } catch (error)
  {
    functions.logger.error(error);
    throw new functions.https.HttpsError("internal", error);
  }
});

/**
 * Cancel an upcoming conference. Removes the conference from the database if present
 *
 * Client must pass:
 * ConferenceId - the id of the upcoming conference to update
 *
 */
exports.cancelUpcomingConference = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid) throw new Error("Not logged in.");

    await db.ref(`Conferences/Upcoming/${data.ConferenceId}`).remove();

    return true;
  }
  catch (error)
  {
    functions.logger.error(error);
    throw new functions.https.HttpsError("internal", error);
  }
});

async function startGivenConference(confId, manualStart)
{
  const oldRef = db.ref("Conferences/Upcoming").child(confId);
  const newRef = db.ref("Conferences/Live").child(confId);

  const scheduledSnap = await oldRef.once("value");

  if (!scheduledSnap.val())
  {
    throw new Error("No conference with that ID is scheduled");
  }

  let newVal = scheduledSnap.val();
  newVal.ActualStart = Date.now();

  // Update the participants with the relevant data
  for (const phoneNumber in newVal.Participants)
  {
    const participant = newVal.Participants[phoneNumber];
    newVal.Participants[phoneNumber] = {
      Moderator: participant.Moderator,
      Name: participant.Name,
      Status: "Invited",
      Joined: false,
      TalkTime: 0,
      SessionData: {
        InCall: false,
        Speaking: false,
      },
    };
  }

  // save the trunk cost model as its state at time the conference started
  const trunk = (await db.ref("System/Trunks").child(newVal.DropCallJoinNumber).once('value')).val();
  newVal.Costing = trunk.codes_cost_per_min;

  await newRef.set(newVal);
  oldRef.set(null);

  const ownerRef = db.ref("Users").child(newVal.Owner);
  const ownerSnap = await ownerRef.once("value");

  if (!ownerSnap.val())
  {
    throw new Error("Couldn't find the conference's owner");
  }

  return requestCallParticipant(confId, ownerSnap.val().Phone, manualStart);
}

/**
 * Start a conference. This involves moving it from 'Upcoming' to 'Live'
 * and updating the 'ActualStart' time to be the time it is called
 *
 * Client must pass:
 * ConferenceId - id of the conference to start
 */
exports.startConference = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.");
    }

    await startGivenConference(data.ConferenceId, true);

    return true;

  } catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError("internal", err);
  }
});

/**
 * Call the given participant in the given conference
 *
 * Client must pass:
 * ConferenceId - the id of the conference to call the participant in
 * PhoneNumber - phone number of the participant to call
 */
exports.callParticipant = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }
    return requestCallParticipant(data.ConferenceId, data.PhoneNumber, true)
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});


/**
* In a given conference, mute/unmute the given participant
*
* To acheive this, the client must pass the:
* ConferenceId
* ParticipantNumber - of participant to mute/unmute
* Uuid - the call uuid of the participant to mute/unmute
* ConfMemId - the conference member id of the participant to mute/unmute
* Mute - whether the participant should be muted or unmuted
*/
exports.muteUnmuteParticipant = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    await AddCallJob(data.ConferenceId,
      data.Muted ? 'mute' : 'unmute',
      data.ParticipantNumber,
      data.Uuid,
      data.ConfMemberId);

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Mute / Unmute all participants in a given conference
 *
 * Client must pass:
 * ConferenceId - the conference to mute/unmute all participants of
 * Mute - whether to mute or unmute
 */
exports.muteUnmuteConference = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    await AddCallJob(data.ConferenceId, data.Mute ? 'muteAll' : 'unmuteAll');

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
* Ban a given participant from the given conference,
* if the participant is in the call, boot them
*
* Client must pass:
* ConferenceId - id of conference to boot the participant from
* ParticipantNumber - the participant number of the participant to boot
*/
exports.banParticipant = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    var conf = (await db.ref(`/Conferences/Live/${data.ConferenceId}`).once("value")).val();

    // If the participant is in the call, boot them
    if (conf.Participants[data.ParticipantNumber].SessionData.InCall && conf.Participants[data.ParticipantNumber].SessionData.CallUuid)
    {

      await AddCallJob(data.ConferenceId,
        'hangup',
        data.ParticipantNumber,
        conf.Participants[data.ParticipantNumber].SessionData.CallUuid);

      functions.logger.log("booted " + data.ParticipantNumber);
    }

    const now = Date.now();

    db.ref(`/Conferences/Live/${data.ConferenceId}/Participants/${data.ParticipantNumber}`).update({
      Banned: now
    })
    db.ref(`/Conferences/Live/${data.ConferenceId}/Events`).push({
      DateTime: now,
      Event: 'ban-participant',
      BannedPhoneNumber: data.ParticipantNumber
    })

  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError(functions.https.funct, err);
  }
});

/**
 * End the given conference
 *
 * Client must pass:
 * ConferenceId - the id of the conference to end
 */
exports.endConference = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    await AddCallJob(data.ConferenceId, 'endConf');

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

// watch for endConf calljobs and move the conference node to 'complete'
exports.moveCompleteConference = functions.database.ref('/Calljobs/Processed/{id}').onCreate(async (snapshot, context) =>
{
  const newLog = snapshot.val()

  if (newLog.Task !== 'endConf') return;

  // wait for a couple of seconds to give FreeSwitch events 
  // time to write to the original db location (e.g. finish recording)
  await new Promise(resolve => setTimeout(resolve, 2000));

  const foundConf = await findConf(newLog.ConferenceId);

  const newConf = foundConf.res.val()
  newConf.ActualEnd = Date.now();

  // move conference from ongoing to 'complete'
  await db.ref(`Conferences/Complete/${newLog.ConferenceId}`).set(newConf);
  await db.ref(`${foundConf.path}`).set(null);
});

/**
 * Rename a given participant within a given conference
 *
 * Client must pass:
 * Conference - the id of the conference to rename the participant within
 * PhoneNumber - the phone number of the participant to rename
 * Name - the new participant name
 */
exports.renameUser = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    let foundConf = await findConf(data.Conference);

    if (!foundConf)
    {
      throw new Error("Conference not found")
    }

    return await db.ref(`${foundConf.path}/Participants/${data.PhoneNumber}`).update({ "Name": data.Name });
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
})


/**
 * Add a given participant to the given conference
 *
 * Client must pass:
 * ConferenceId - the ID of the conference to add the participant to
 * PhoneNumber - the phone number of the participant
 * Name - the name of the new participant
 * Moderator - whether the new participant is moderator
 */
exports.addParticipant = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.ConferenceId || !data.PhoneNumber || !data.Name)
    {
      throw new Error("Requires conference ID and new participant's phone number and name.")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found with ID " + data.ConferenceId);
    }

    return await db.ref(`${foundConf.path}/Participants/${data.PhoneNumber}`).update(
      {
        "Moderator": data.Moderator,
        "Name": data.Name,
        "Status": "Invited",
        "Joined": false,
        "TalkTime": 0,
        "SessionData": {
          "InCall": false,
          "Speaking": false
        }
      });
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Remove a participant from an upcoming conference.
 *
 * This function only works on upcoming conferences, as it
 * completely removes a participant and all associated data
 * from a conference.
 *
 * Once a participant is live, participants can only banned,
 * as banning a participant doesn't remove the associatd data
 *
 * Client must pass:
 * ConferenceId - the id of the conference to remove the participant from
 * PhoneNumber - the participant of the user to remove
 */
exports.removeParticipant = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid) throw new Error("Not logged in.");

    const participantRef = db.ref(`Conferences/Upcoming/${data.ConferenceId}/Participants/${data.PhoneNumber}`);

    const participantSnapshot = await participantRef.once("value");

    if (!participantSnapshot.exists())
      throw new Error("This Participant doesn't exist");

    await participantRef.set(null)
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

// Get necessary info to connect to FreeSwitch via webrtc
// Client must pass:
// ConferenceId - id of the conference the webrtc instance will access
exports.getWSinfo = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.");
    }

    return {
      FreeswitchWS: env.deployment.freeswitchws,
      FreeswitchUserPW: env.deployment.freeswitchuserpw,
      FreeswitchUserAddress: env.deployment.freeswitchuseraddress,
      FreeswitchExtraData: data.ConferenceId + '%%' + context.auth.uid
    }

  } catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError("internal", err);
  }
});

/**
 * Update the agenda of the given conference
 *
 * Client must pass:
 * ConferenceId - the ID of the conference to add the participant to
 * Agenda - an array of agenda items
 */
exports.updateConferenceAgenda = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.ConferenceId || !data.Agenda)
    {
      throw new Error("Requires conference ID and agenda data.")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found with ID " + data.ConferenceId);
    }

    return await db.ref(`${foundConf.path}/Agenda`).set(data.Agenda);
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Add a minute note to an agenda item during the call
 *
 * Client must pass:
 * ConferenceId - the ID of the conference
 * AgendaItemIndex - The index of the current agenda item
 * Text - the content of the note
 */
exports.addMinuteNoteToAgendaItem = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.ConferenceId || typeof data.AgendaItemIndex === 'undefined' || !data.Text)
    {
      throw new Error("Requires conference ID, AgendaItemIndex, and the Text of the note.")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found with ID " + data.ConferenceId);
    }

    let databaseOps = [];

    databaseOps.push(db.ref(`${foundConf.path}/Agenda/${data.AgendaItemIndex}/MinuteNotes/`).push({
      Text: data.Text,
      DateTime: Date.now()
    }));

    databaseOps.push(db.ref(`${foundConf.path}/Events`).push({
      DateTime: Date.now(),
      Event: 'agenda-note-add',
      AgendaItem: data.AgendaItemIndex,
      Text: data.Text
    }));

    await Promise.allSettled(databaseOps);

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Delete a minute note from an agenda item during the call
 *
 * Client must pass:
 * ConferenceId - the ID of the conference
 * AgendaItemIndex - The index of the current agenda item
 * NoteId - The id of the note to remove
 */
exports.deleteMinuteNoteFromAgendaItem = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.ConferenceId || typeof data.AgendaItemIndex === 'undefined' || !data.NoteId)
    {
      throw new Error("Requires conference ID, AgendaItemIndex, and the ID of the note to delete.")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found with ID " + data.ConferenceId);
    }

    const noteText = foundConf.res.val().Agenda[data.AgendaItemIndex].MinuteNotes[data.NoteId].Text;

    let databaseOps = [];

    databaseOps.push(db.ref(`${foundConf.path}/Agenda/${data.AgendaItemIndex}/MinuteNotes/${data.NoteId}`).set(null));

    databaseOps.push(db.ref(`${foundConf.path}/Events`).push({
      DateTime: Date.now(),
      Event: 'agenda-note-delete',
      AgendaItem: data.AgendaItemIndex,
      Text: noteText
    }));

    await Promise.allSettled(databaseOps);

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Advance the conference agenda
 *
 * Client must pass:
 * ConferenceId - the ID of the conference
 */
exports.advanceAgenda = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.ConferenceId)
    {
      throw new Error("Requires conference ID")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found with ID " + data.ConferenceId);
    }

    let thisConf = foundConf.res.val();
    let newIndex = typeof thisConf.AgendaCurrent === 'undefined' ? 0 : thisConf.AgendaCurrent + 1;

    if (!thisConf.Agenda || newIndex >= thisConf.Agenda.length)
    {
      throw new Error("Agenda couldn't be advanced - check that this conference has an agenda, " +
        "and that you're not trying to extend out of bounds.");
    }

    let databaseOps = [];
    databaseOps.push(db.ref(`${foundConf.path}/Agenda/${newIndex}/Started`).set(Date.now()));
    databaseOps.push(db.ref(`${foundConf.path}/AgendaCurrent`).set(newIndex));
    databaseOps.push(db.ref(`${foundConf.path}/Events`).push({
      DateTime: Date.now(),
      Event: 'agenda-advance',
      AgendaItem: newIndex
    }));

    await Promise.allSettled(databaseOps);

    const owner = await (await db.ref("Users").child(thisConf.Owner).once("value")).val();

    let promises = [];

    // make sure that all participants are appropriately muted/unmuted
    for (const num in thisConf.Participants)
    {
      const participant = thisConf.Participants[num];
      if (participant.Status !== 'Active') continue;

      const muted = (owner.Phone === num ||
        (thisConf.Agenda[newIndex].Attendees && thisConf.Agenda[newIndex].Attendees.includes(num))) ? false : true;

      if (thisConf.Participants[num].Muted !== muted)
      {
        promises.push(AddCallJob(data.ConferenceId,
          muted ? 'mute' : 'unmute', num,
          participant.SessionData.CallUuid,
          participant.SessionData.ConfMemberId));
      }
    }

    await Promise.allSettled(promises);

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.dismissHandUp = functions.https.onCall(async (data, context) =>
{
  try
  {
    if (!context.auth || !context.auth.uid) throw new Error("Not logged in.");

    let foundConf = await findConf(data.ConferenceId);

    const participantRef = db.ref(`${foundConf.path}/Participants/${data.ParticipantNumber}`);
    const participantSnapshot = await participantRef.once("value");

    if (!participantSnapshot.exists())
    {
      throw new Error("This Participant doesn't exist");
    }

    await participantRef.update({ 'HandUp': false });

    db.ref(`${foundConf.path}/Events`).push({
      DateTime: Date.now(),
      Event: 'dismiss-hand',
      Number: data.ParticipantNumber,
      ContactName: participantSnapshot.val().Name
    })
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Register the given anonymous user with their uid and their password attempt
 * RTDB rules will only allow read access if the uid and password matches
 * 
 * Client must pass:
 * ConferenceId - the id of the conference to call the participant in
 * Password - the password to attempt guest access with
 */
exports.addAnonymousConfAndPwd = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }
    await db.ref(`AnonymousUsers/${context.auth.uid}`).update({ [data.ConferenceId]: data.Password });
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.startPoll = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (!data.Conference || !data.Title || data.IsAnon === null || !data.Options)
    {
      throw new Error("Invalid data format")
    }

    let foundConf = await findConf(data.Conference);

    if (!foundConf)
    {
      throw new Error("Conference not found")
    }

    let poll = {
      'Title': data.Title,
      'Anonymous': data.IsAnon,
      'Open': true,
      'Options': []
    };

    for (let i = 0; i < data.Options.length; i++)
    {
      poll.Options.push({
        'Text': data.Options[i].Text
      });
    }

    let startTime = Date.now();

    await db.ref(`${foundConf.path}/Polls/${startTime}`).set(poll);

    db.ref(`${foundConf.path}/Events`).push({
      DateTime: startTime,
      Event: 'start-poll',
      Title: data.Title
    })
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.endPoll = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    let foundConf = await findConf(data.ConferenceId);

    if (!foundConf)
    {
      throw new Error("Conference not found")
    }

    let thisConf = foundConf.res.val();

    if (!thisConf.Polls)
    {
      throw new Error("Poll not found")
    }

    let pollIds = Object.keys(thisConf.Polls);
    let lastPoll = pollIds[pollIds.length - 1];

    await db.ref(`${foundConf.path}/Polls/${lastPoll}`).update({ 'Open': false });

    db.ref(`${foundConf.path}/Events`).push({
      DateTime: Date.now(),
      Event: 'end-poll',
      Title: thisConf.Polls[lastPoll].Title
    })
  }
  catch (err)
  {
    functions.logger.error(err);
    throw new functions.https.HttpsError('internal', err);
  }
});

 
exports.hangupParticipant = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }
    await AddCallJob(data.ConferenceId,
      'hangup',
      data.ParticipantNumber,
      data.Uuid);
    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});