const functions = require('firebase-functions');
const path = require('path');
const os = require('os');
var fs = require('fs');
const lame = require("@suldashi/lame");
var wav = require('wav');
const admin = require('firebase-admin');
const db = admin.database();
const storage = admin.storage();

async function findConf(confId)
{
  var thisConf = await db.ref(`Conferences/Live/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Live/${confId}`, 'res': thisConf };

  thisConf = await db.ref(`Conferences/Upcoming/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Upcoming/${confId}`, 'res': thisConf };

  thisConf = await db.ref(`Conferences/Complete/${confId}`).once("value");
  if (thisConf.exists()) return { 'path': `Conferences/Complete/${confId}`, 'res': thisConf };

  return null;
}

exports.generateMP3 = functions.storage.object().onFinalize(async (object) =>
{
  try
  {
    if (!object.contentType.startsWith('audio/'))
    {
      return functions.logger.log('This is not an audio file.');
    }

    const filePath = object.name;

    if (!filePath.includes('recordings'))
    {
      return functions.logger.log('This is not a recording.');
    }

    const fileName = path.basename(filePath);
    if (fileName.endsWith('.mp3'))
    {
      return functions.logger.log('Already an mp3.');
    }

    // Download file from bucket.
    const bucket = storage.bucket();
    const wavLocalPath = path.join(os.tmpdir(), fileName);

    await bucket.file(filePath).download({ destination: wavLocalPath });

    const filenameWithoutSuffix = fileName.substring(0, fileName.length - 4)
    const newFileName = filenameWithoutSuffix + ".mp3";
    const mp3LocalPath = path.join(os.tmpdir(), newFileName);

    const input = fs.createReadStream(wavLocalPath);
    const output = fs.createWriteStream(mp3LocalPath);

    // start reading the WAV file from the input
    var reader = new wav.Reader();

    // we have to wait for the "format" event before we can start encoding
    reader.on('format', (format) =>
    {
      try
      {
        functions.logger.debug(`WAV format: ${format}`)

        // encode the wave file into an MP3 by calling pipe()
        var encoder = new lame.Encoder(format);
        reader.pipe(encoder).pipe(output);
      }
      catch (error)
      {
        functions.logger.error(error);
      }
    });

    output.once('close', async () =>
    {
      try 
      {
        const mp3RemotePath = path.join(path.dirname(filePath), newFileName);
        // upload the new file
        await bucket.upload(mp3LocalPath, {
          destination: mp3RemotePath,
          metadata: {
            contentType: 'audio/mp3',
          }
        });

        functions.logger.debug('Uploaded MP3')

        functions.logger.debug('Remote filepath: ' + filePath)

        // delete the local files to free up disk space.
        fs.unlinkSync(mp3LocalPath);
        fs.unlinkSync(wavLocalPath);

        await bucket.file(filePath).delete();

        functions.logger.debug('Cleaned up WAV')

        // need to get conference info, and check
        // if this is a participant recording or the whole conference one
        const pathSegments = filePath.split(path.sep)

        functions.logger.debug(pathSegments)

        if (pathSegments[0] !== 'recordings')
        {
          return;
        }

        const confId = pathSegments[2];
        const foundConf = await findConf(confId);

        // initial participant recordings are stored with the following path:
        // /recordings/HOST_ID/CONFERENCE_ID/PARTICIPANT_NUMBER/filename.wav
        // initial full conference recordings are stored with the following path:
        // /recordings/HOST_ID/CONFERENCE_ID/conference/filename.wav

        // if we are only one level deep into the 'recordings' directory, 
        // this directory name is the conference ID and this is the conference recording 
        if (pathSegments[0] === 'recordings' && pathSegments[3] === 'conference')
        {
          functions.logger.debug('Conference recording')
          // add MP3 address to db
          await db.ref(`${foundConf.path}/Recordings/Conference/${filenameWithoutSuffix}`).update(
            {
              'Upload': mp3RemotePath,
              'Processing': null,
              'LocalPath': null
            });
        }
        else if (pathSegments[0] === 'recordings' && pathSegments.length === 5)
        {
          functions.logger.debug('Participant recording')
          // this is a participant recording. Recording ID is a timestamp, same as the filename
          await db.ref(`${foundConf.path}/Recordings/Participants/${pathSegments[3]}/${filenameWithoutSuffix}`).update(
            {
              'Upload': mp3RemotePath,
              'Processing': null,
              'LocalPath': null
            });
        }
      }
      catch (error) 
      {
        functions.logger.error(error)
      }
    })

    // start transferring the data
    input.pipe(reader);
  }
  catch (error) 
  {
    functions.logger.error(error)
  }

  return true;
});