const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.database();
const auth = admin.auth();
const env = functions.config();
const companyEmailValidator = require("company-email-validator");

// Makes the currently logged in user an admin
// Requires the correct password is passed.
exports.elevateCurrentUser = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    if (data.Password !== env.deployment.adminpassword) 
    {
      throw new Error("Wrong password")
    }

    await auth.setCustomUserClaims(context.auth.uid, { admin: true })

    await db.ref(`Users/${context.auth.uid}`).update(
      {
        'IsAdmin': true
      });

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.updateUserAdminStatus = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    let isAdmin = (await auth.getUser(context.auth.uid)).customClaims['admin']
    if (!isAdmin) throw new Error("You do not have permissions to do that.")

    await auth.setCustomUserClaims(data.Id, { admin: data.IsAdmin })

    const thisUser = await db.ref(`Users/${data.Id}`).once("value");
    const userVal = thisUser.val();
    if (userVal.IsSuperAdmin && !data.IsAdmin)
    {
      throw new Error("Super Admins must be manually removed through the database.")
    }

    await db.ref(`Users/${data.Id}`).update(
      {
        'IsAdmin': data.IsAdmin
      });

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

// Adds a new domain to the allowed list of emails usable for account creation
exports.addAllowedDomain = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    let isAdmin = (await auth.getUser(context.auth.uid)).customClaims['admin']
    if (!isAdmin) throw new Error("You do not have permissions to do that.")

    let testAddress = 'test@' + data.Domain;
    let supported = companyEmailValidator.isCompanyEmail(testAddress)

    if (!supported)
    {
      throw new Error("Please do not add domains of free email providers.")
    }

    let sanitised = data.Domain.replace(/\./g, ',');

    await db.ref(`System/Allowed/Domains/${sanitised.toLowerCase()}`).update(
      {
        'AddedBy': context.auth.uid,
        'LastModified': Date.now()
      });

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.updateTrunk = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    var isAdmin = (await auth.getUser(context.auth.uid)).customClaims['admin']
    if (!isAdmin) throw new Error("You do not have permissions to do that.")

    await db.ref(`System/Trunks/${data.trunkId}`).update(
      {
        'codes_cost_per_min': data.supportedCodes,
        'capacity': data.capacity,
        'removeIntCode': data.removeIntCode,
        'addZeroToLocal': data.addZeroToLocal,
        'updated': Date.now()
      });

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

exports.addAllowedEmail = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    var isAdmin = (await auth.getUser(context.auth.uid)).customClaims['admin']
    if (!isAdmin) throw new Error("You do not have permissions to do that.")

    let sanitised = data.Email.replace(/\./g, ',');

    await db.ref(`System/Allowed/Emails/${sanitised.toLowerCase()}`).update(
      {
        'AddedBy': context.auth.uid,
        'LastModified': Date.now()
      });

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});
