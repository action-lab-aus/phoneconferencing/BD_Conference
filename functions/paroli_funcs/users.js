const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.database();
const auth = admin.auth();
const env = functions.config();
const sgMail = require("@sendgrid/mail");

function sendEmail(toAddress, subject, body)
{
  const msg = {
    to: toAddress,
    from: env.deployment.adminemail,
    subject: subject,
    text: body,
    html: body
  };
  sgMail.setApiKey(env.deployment.sendgridapi);
  sgMail.send(msg);
}

/**
 * Create a new User
 *
 * Client must pass:
 * Name - The name of the new user
 * Email - The email of the new user
 * Password - The user's new password
 * Phone - The phone number of the new user
 * Country - The new user's country
 * Language - The new user's preferred language
 * EmailSubject - The subjectline of the account confirmation email
 * EmailBody - The body of the account confirmation email, containing the text {CONFIRMATION_LINK} which will be replaced
 */
exports.createUser = functions.https.onCall(async (data, context) =>
{
  let result = {
    success: false,
    error: null,
    user: null
  }

  try
  {
    let attemptedEmail = data.Email;
    let sanitised = attemptedEmail.replace(/\./g, ',').toLowerCase();

    let allowed = (await db.ref(`System/Allowed/Emails/${sanitised}`).once("value")).val();

    if (!allowed)
    {
      // email isn't on the whitelist, check if the email's domain is
      let attemptedDomain = sanitised.split('@')[1];
      allowed = (await db.ref(`System/Allowed/Domains/${attemptedDomain}`).once("value")).val();

      if (!allowed)
      {
        // new number who dis? GTFO
        functions.logger.warn("Registration attempt without whitelist permission: " + attemptedEmail);
        result.error = "Apologies, your email address does not have access to this service.";
        return result;
      }
    }

    functions.logger.log("Valid new address: " + attemptedEmail);

    let newUser = await auth.createUser({
      email: data.Email,
      emailVerified: false,
      password: data.Password,
      displayName: data.Name,
      disabled: false,
    })

    functions.logger.log("Created user " + newUser.uid);

    let confirmationLink = await auth.generateEmailVerificationLink(data.Email, {
      url: env.deployment.website
    })

    let body = data.EmailBody.replace('{CONFIRMATION_LINK}', `<br><a href="${confirmationLink}">${confirmationLink}</a>`)

    sendEmail(data.Email, data.EmailSubject, body);

    functions.logger.log("Sent email");

    await db.ref(`Users/${newUser.uid}`).update(
      {
        Name: data.Name,
        Email: data.Email,
        Phone: data.Phone,
        Country: data.Country,
        Language: data.Language
      });

    await db.ref(`Users/${newUser.uid}/Contacts/${data.Phone}`).update(
      {
        Name: data.Name,
        Created: Date.now()
      });

    result.success = true;
    result.user = newUser;

    functions.logger.log("Finished createUser", result);

    return result;
  }
  catch (err)
  {
    functions.logger.error(err);
    result.success = false;
    result.error = err.errorInfo ? err.errorInfo.message : err;
    return result;
  }
});

/**
* Resend the confirmation email
*
* Client must pass:
* Email - The email of the new user
* EmailSubject - The subjectline of the account confirmation email
* EmailBody - The body of the account confirmation email, containing the text {CONFIRMATION_LINK} which will be replaced
*/
exports.resendConfirmationEmail = functions.https.onCall(async (data, context) => 
{
  try
  {

    let found = await auth.getUserByEmail(data.Email)

    if (!found)
    {
      // don't let on that this isn't a registered email
      return true;
    }

    let confirmationLink = await auth.generateEmailVerificationLink(data.Email, {
      url: env.deployment.website
    })

    let body = data.EmailBody.replace('{CONFIRMATION_LINK}', `<br><a href="${confirmationLink}">${confirmationLink}</a>`)

    sendEmail(data.Email, data.EmailSubject, body);

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
});

/**
 * Create a contact
 *
 * Client must pass:
 * Name - name of the new contact
 * Number - the number of the new contact
 */
exports.createContact = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    var contact = {
      "Name": data.Name,
      "Created": Date.now()
    }

    await db.ref(`/Users/${context.auth.uid}/Contacts/${data.Number}`).update(contact);

    return contact;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
})

/**
 * Delete a contact
 *
 * The only data required is the number of the contact to delete
 *
 */
exports.deleteContact = functions.https.onCall(async (number, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    await db.ref(`/Users/${context.auth.uid}/Contacts/${number}`).remove();

    // also need to remove this contact from the user's groups

    const groups = await db.ref(`/Users/${context.auth.uid}/Groups`).once("value");

    if (groups.exists())
    {
      const existingGroups = groups.val();

      for (const group in existingGroups)
      {
        if (Object.prototype.hasOwnProperty.call(existingGroups[group].Members, number))
        {
          if (Object.keys(existingGroups[group].Members).length === 1)
          {
            // this is the only member of this group, just delete the whole group
            db.ref(`/Users/${context.auth.uid}/Groups/${group}`).remove();
          }
          else
          {
            db.ref(`/Users/${context.auth.uid}/Groups/${group}/Members/${number}`).remove();
          }

        }
      }
    }

    return true;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
})

/**
 * Create or update a group of contacts
 *
 * Client must pass:
 * Name - name of the new group (will be used as ID)
 * Members - dictionary of members, numbers as keys
 */
exports.createGroup = functions.https.onCall(async (data, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    const group = {
      "Name": data.Name,
      "Created": Date.now(),
      "Members": data.Members
    }

    const newId = group.Name.toLowerCase();

    await db.ref(`/Users/${context.auth.uid}/Groups/${newId}`).set(group);

    if (data.OriginalName && data.OriginalName.toLowerCase() !== newId)
    {
      // the name has changed, remove the node of the old name
      await db.ref(`/Users/${context.auth.uid}/Groups/${data.OriginalName.toLowerCase()}`).remove();
    }

    return group;
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
})

/**
 * Delete a group
 *
 * Requires the name of the group to delete
 */
exports.deleteGroup = functions.https.onCall(async (name, context) => 
{
  try
  {
    if (!context.auth || !context.auth.uid)
    {
      throw new Error("Not logged in.")
    }

    return await db.ref(`/Users/${context.auth.uid}/Groups/${name.toLowerCase()}`).remove();
  }
  catch (err)
  {
    functions.logger.error(err);
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('internal', err);
  }
})