const admin = require('firebase-admin');
admin.initializeApp();

const adminFuncs = require("./paroli_funcs/admin");
exports.updateUserAdminStatus = adminFuncs.updateUserAdminStatus;
exports.elevateCurrentUser = adminFuncs.elevateCurrentUser;
exports.addAllowedDomain = adminFuncs.addAllowedDomain;
exports.addAllowedEmail = adminFuncs.addAllowedEmail;
exports.updateTrunk = adminFuncs.updateTrunk;

const userFuncs = require("./paroli_funcs/users");
exports.createUser = userFuncs.createUser;
exports.resendConfirmationEmail = userFuncs.resendConfirmationEmail;
exports.createContact = userFuncs.createContact;
exports.deleteContact = userFuncs.deleteContact;
exports.createGroup = userFuncs.createGroup;
exports.deleteGroup = userFuncs.deleteGroup;

const conferenceFuncs = require("./paroli_funcs/conferences");
exports.getUpcomingConferences = conferenceFuncs.getUpcomingConferences;
exports.checkForBeginAndEnd = conferenceFuncs.checkForBeginAndEnd;
exports.scheduleConference = conferenceFuncs.scheduleConference;
exports.updateConference = conferenceFuncs.updateConference;
exports.cancelUpcomingConference = conferenceFuncs.cancelUpcomingConference;
exports.muteUnmuteParticipant = conferenceFuncs.muteUnmuteParticipant;
exports.muteUnmuteConference = conferenceFuncs.muteUnmuteConference;
exports.banParticipant = conferenceFuncs.banParticipant;
exports.endConference = conferenceFuncs.endConference;
exports.moveCompleteConference = conferenceFuncs.moveCompleteConference;
exports.renameUser = conferenceFuncs.renameUser;
exports.getWSinfo = conferenceFuncs.getWSinfo;
exports.startConference = conferenceFuncs.startConference;
exports.updateConferenceAgenda = conferenceFuncs.updateConferenceAgenda;
exports.addMinuteNoteToAgendaItem = conferenceFuncs.addMinuteNoteToAgendaItem;
exports.deleteMinuteNoteFromAgendaItem = conferenceFuncs.deleteMinuteNoteFromAgendaItem;
exports.addParticipant = conferenceFuncs.addParticipant;
exports.removeParticipant = conferenceFuncs.removeParticipant;
exports.advanceAgenda = conferenceFuncs.advanceAgenda;
exports.dismissHandUp = conferenceFuncs.dismissHandUp;
exports.addAnonymousConfAndPwd = conferenceFuncs.addAnonymousConfAndPwd;
exports.callParticipant = conferenceFuncs.callParticipant;
exports.startPoll = conferenceFuncs.startPoll;
exports.endPoll = conferenceFuncs.endPoll;
exports.hangupParticipant = conferenceFuncs.hangupParticipant;

const storageFuncs = require("./paroli_funcs/storage");
exports.generateMP3 = storageFuncs.generateMP3;