# BD_Conference

Requires environment configuration - see .runtimeconfig.json. To set a variable:

```
firebase functions:config:set someservice.key="THE API KEY" someservice.id="THE CLIENT ID"
```

To pipe the current project's config into a file, use:

```
firebase functions:config:get > .filename.json
//or, if powershell:
firebase functions:config:get | ac .filename.json
```

To deploy:

```
firebase deploy --only functions
```